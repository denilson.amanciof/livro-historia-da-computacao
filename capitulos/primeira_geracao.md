# Primeira Geração

É difícil dizer com segurança qual foi realmente o primeiro computador da história e a disputa para ocupar esse posto é acirrada. Até mesmo muitos especialistas em tecnologia, historiadores e a imprensa nunca entrou em um consenso que chegasse a nomear algumas das primeiras máquinas como “o primeiro computador da história”. Dito isto, o que se pode fazer é destacar algumas máquinas que foram importantes para o desenvolvimento do que conhecemos hoje como computador. Chamamos esse primeiro grupo de máquinas colossais, primeira geração.

</br></br>

## ENIAC - (1946)

  <img src = "images/eniac-o-primeiro-computador.jpeg">

O ENIAC é considerado um dos primeiros computadores programáveis da história, por isso tamanha importância em comparação a computadores que podem ter surgido até mesmo antes dele, porém, que apenas realizavam um ou outro tipo de cálculo. O ENIAC se destaca, justamente por ser pioneiro na categoria de máquinas digitais de uso geral. Esse computador foi desenvolvido ainda durante a Segunda Guerra Mundial, sendo atribuído aos cientístas John Mauchly e John Presper Eckert a engenharia de sua criação. No início o computador tinha o intuito de auxiliar o exército americano, calculando rotas de misseis, analisando variáveis com muita precisão, o que levaria dias para se fazer com calculos realizados por um ser humano. A sua estrutura ocupava um sala inteira, contando com 40 unidades de painéis, 17.468 tubos de vácuo, 1500 interruptores, consumo de 150 kilowatts de energia e peso de cerca de 30 toneladas [1]. Assim, o ENIAC serviu aos Estados Unidos principalmente na pesquisa balística, até início da Guerra Fria, quando seu uso deixou de ser viável por já existirem outros equipamentos, mais baratos e rápidos. Um fato interessante é destacar que muitas mulheres estavam concetadas ao desenvolvimento e funcionamento do ENIAC, elas foram as primeiras programadoras da máquina e auxiliaram no seu melhoramento no decorrer do tempo, dentre elas, podemos citar nomes como Frances Bilas, Jean Jennings, Ruth Lichterman, Kathleen McNulty, Betty Snyder, Marlyn Wescoff, entre outras [2].

</br> </br>

## IBM 701 - (1953)

 <img src = "images/ibm701.jpg"> 

O IBM 701 foi um dos primeiros computadores criados por uma empresa realmente do ramo de computadores, a [IBM](https://www.ibm.com/br-pt), que ainda existe e está operante em pesquisas e no desenvolvimento da Ciência da Computação. Esta máquina foi criada graças a insistência de Thomas Johnson Watson Junior, filho do lendário gerente da IBM. Segundo a empresa, o IBM 701 foi o primeiro computador de uso geral com sucesso comercial verdadeiramente lucrativo [5]. A princípio sua invenção inicicou-se com intuito de também auxiliar a Coréia do Sul na Guerra da Coréia, cujo período de duração foi de 1950 até o ano da criação do computador, as suas principais funções eram a capacidade de armazenar grande quantidade de informação e analisá-las com precisão. Entretanto, o uso do IBM 701 não parou com o fim da guerra, ao todo foram produzidos dezenove máquinas, sendo distribuidos pelo país entre laboratórios de pesquisa, empresas de aeronaves, marinha e outras agências governamentais americanas [3]. Na época o aluguel dos IBM 701 custava certa de 8 mil dólares [5], o que foi um grande passo para a empresa e também para história do desenvolvimento de computadores.

</br> </br></br>

-----

# Referências:

1. KLEINA, Nilton. A história do ENIAC, um dos pais dos computadores. TecMundo. 2018. Disponível em: < https://www.tecmundo.com.br/mercado/135693-historia-eniac-pais-dos-computadores-video.htm >.

1. Fritz, W. B. "The women of ENIAC," in IEEE Annals of the History of Computing, vol. 18, no. 3, pp. 13-28, Fall 1996.

1. DRUM, Marculi. A História da IBM. Oficina da Net. Tecnologia. 09 mar. 2018. Disponível em: < https://www.oficinadanet.com.br/post/16128-historia-ibm >.

1. KLEINA, Nilton. A história da IBM, uma empresa centenária e revolucionária [em vídeo]. TecMundo. 31 out. 2017. Disponível em: < https://www.tecmundo.com.br/mercado/121588-historia-ibm-empresa-centenaria-revolucionaria-video.htm >.

1. HURD, Cuthbert C. Computer Development at IBM in A History of Computing in the Twentieth Century. p. 389-418, Academic Press, 1980.

1. IBM 701
Electronic analytical control unit <https://www.ibm.com/ibm/history/exhibits/701/701_141502.html>

