# Computação Móvel

<img src = "images/computacaomovel.png">

Dos anos 90 para cá, pudemos notar um grande crescimento no desenvolvimento de tecnologias para comunicação celular móvel,
comunicação via satélite e redes locais sem fio. A popularização dessas tecnologias tem permitido o acesso a informações remotas onde quer que se esteja, abrindo um leque muito grande de facilidades, aplicações e serviços para os usuários.

Nota-se também, uma grande evolução e popularização de dispositivos computacionais móveis, tais como celulares, PDAs (Personal
Digital Assistants) e laptops, que nos traz a estimativa de que em poucos anos milhares de pessoas espalhadas pelo mundo terão um
desses tipos de dispositivos com a capacidade de comunicação com as redes fixas tradicionais e com outros computadores móveis. Esse ambiente propicia a criação do conceito de computação móvel.

**Computação móvel** pode ser representada como um novo paradigma computacional que permite que usuários desse ambiente tenham acesso a serviços independentemente de sua localização, podendo inclusive, estar em movimento. Mais tecnicamente, é um conceito que envolve processamento, mobilidade e comunicação sem fio. A idéia é ter acesso à informação em
qualquer lugar e a qualquer momento.

O avanço da tecnologia de armazenamento na nuvem, a redução de custo do hardware, a expansão da telefonia celular (WAN sem fio) e em redes Wi-Fi (LAN sem fio) permitiu que praticamente todas as áreas de negócios e serviços utilizem os sistemas mobile e seus mais diversos dispositivos: tablet, smartphones, PDV, contactless, aparelhos IoT (Internet das Coisas) e Wearables. Esses aplicativos e dispositivos estão se aprimorando a cada dia e vamos ver agora onde essa tecnologia já está sendo utilizada.

**Indústria**
**Projeto Crema**

Criado com o objetivo de integrar cloud computing na indústria, já possui diversas aplicações em produção. Uma delas é utilizada pela GOIZPER, principal fabricante de freio de embreagem na Espanha. As máquinas foram automatizadas para monitorar o desempenho da produção e suas linhas de processo, além de analisar possíveis problemas de negócios. São alarmes inteligentes definidos pela equipe de engenharia que realiza análises para calcular os KPIs relacionados ao freio da embreagem e sinaliza a área em caso de problemas.

**Inovações na Saúde
Vein Finder**

Um sistema para localizar via infravermelho as veias e vasos difíceis de identificar a olho nu. O aparelho portátil permite que o operador trabalhe com as mãos livres, bastando aproximá-lo a 30 cm da pele. Fabricado no Brasil, o método não invasivo oferece imagens em tempo real e permite documentação de imagens, dados do paciente, além de observações em PDF.

**Embrace Watch**

Indo na onda dos wearables, que são dispositivos que podem ser utilizados como peças do vestuário como óculos, relógios ou camisas, surgiu o Embrace, um relógio bem bonito, elegante e fácil de usar, mas que antes disso tem uma função primordial na vida de muitas pessoas: ele pode detectar um ataque epiléptico ou uma convulsão e pedir socorro imediato.

**Sistemas mobile no Ensino
Mobile Maths**

Desenvolvido pela Mobile-Science, é um sistema para ensino de matemática via mobile. Também plota gráficos de funções 2D e 3D, faz cálculos estatísticos, polinomiais, matriciais, integrais, derivadas.

As aplicações da computação móvel hoje se tornaram onipresentes e abrangentes ao consumidor, nas empresas, nas vendas, na indústria, na saúde, na segurança, no entretenimento e em muitas outras atividades especializadas de mercado vertical.[1].

# Referências:

1. O que é computação móvel e como você pode fazer uso dessa tecnologia? <http://logicalminds.com.br/o-que-e-computacao-movel-e-como-voce-pode-fazer-uso-dessa-tecnologia/>
