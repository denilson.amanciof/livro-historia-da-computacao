# Quarta Geração

Da década de 70 existiam extremos no mundo dos computadores. As grandes empresas e instituições de pesquisa utilizavam ainda os Mainframes, que ainda ocupavam um grande espaço e eram de alto custo. Por outro lado, haviam computadores que eram produzidos de forma independente por hobbystas, apaixonados por computadores que começaram a dar início aos minicomputadores, kits que podiam ser montados de forma independe que custavam muito mais barato e ocupavam muito menos espaço, tendo funcionalidades básicas.

</br></br>

## Altair 8800 - (1971)
                
<img src = "images/Altair_8800.jpg">

Este computador é considerado um símbolo do início da microinformática. Utilizando a CPU Intel 8080, um processador de 8 bits, com 6000 transístores, Ed Roberts co-fundador da MITS, juntamente de Forrest M. Mims, começou a montar um computador de processamento muito rápido para o seu tamanho. O projeto do Altair 8800 tinha a inteção de ser simples o bastante para que um hobbysta com experiência mediana conseguisse montá-lo e um preço mais acessível, já que os milhoes de dólares cobrados pelos Mainframes eram distantes da realidade de um montador de computadores amador. A MITS, com o projeto do computador desenvolvido por Ed. Roberts começou então a fabricar essas máquinas com a intenção de apresentá-las aos hobbystas e a grandes empresas que poderiam produzí-los em massa. Um fato interessante é que a primeira vez que o computador foi mostrado ao público foi pela revista Popular Eletronics, porém, quando Ed. Roberts voava para Nova York com a intenção de apresentar seu projeto à revista, o computador foi roubado  [1]. Mesmo já sendo peça de museu hoje em dia, o Altair 8800 é considerado um dos precursores da era do computador doméstico.

</br></br></br>

## Apple 1 - (1976)

<img src = "images/ap1.jpeg"> 

O computador criado por Steve Wosniak, co-fundador da Apple junto de Steve jobs, é considerado por muitos o primeiro computador pessoal da história vendido já montado (uma vez que o Altair 8800 podia ser adquirido como Kit, ou já montado por um preço adicional). O Apple I é uma máquina que a primeira vista pouco se parece com um computador, pois não possuia monitor, era basicamente uma placa uma estrutura de madeira, que realizava processamentos básicos. Projetado por Wosniak com peças descartadas por grandes corporações na região de Palo Alto, na Califórnia, o Apple I foi a primeira parceria de sucesso entre os dois Steves, sendo Jobs o responsável pelas primeiras vendas feitas do dispositivo. O primeiro contrato assinado pela para produzir o Apple I previa 50 unidades do aparelho [2]. No ano seguinte, seria fundada a Apple Computer, revolucionando a história dos computadores pessoais.

</br></br></br>

## Apple 2 - (1977)

<img src = "images/apple2.jpg"> 

Logo após a concretização contratual da empresa, Steve Jobs e Steve Wosniak criam o Apple II, sendo o primeiro grande sucesso de vendas da companhia. O computador contava com um corpo de plástico mais elegante que o de seu sucessor e era capaz de processar gráficos coloridos, daí o motivo pelo qual a primeira logomarca da empresa possuir todas as cores do arco-íris. Ao longo dos anos, o dispositivo ganhou diversas versões e atualizações, com upgrades de componentes e modificações em seu sistema [2]. O computador logo caiu nas graças do público, especialmente no mundo corporativo, devido a sua capacidade de processar e armazenar dados em planilhas. O preço do Apple II variava de 1300 à 2600 dólares, valor que era equivalente ao triplo do seu antecessor, no mínimo.

</br></br></br>

## Acorn Archimedes - (1987)

<img src = "images/Acornacrchimedes.jpg"> 

Na década de 1980 a BBC (British Broadcast Corporation) gigante empresa britânica contratou a Acorn Digital Computers, com a intenção de desenvolver uma nova arquitetura para microcomputadores, inicialmente chamado de BBC computer literacy project, depois dando início ao núcleo de processamento ARM, hoje muito conhecido. O projeto foi um sucesso na implementação de um novo tipo de processador comercial [3]. A série de computadores Archimedes que foi um sucesso da Acorn Computers chegou a contar com quatro slots de expansão de memória em uma de suas versões.


</br></br>

# Referências:

1. CARDOSO, Carlos. Altair 8800: 45 anos do computador que deu início à Revolução Digital. Tecnoblog. 2020. Disponível em: < https://tecnoblog.net/meiobit/433555/altair-8800-45-anos-do-computador-que-deu-inicio-a-revolucao-digital/>.

1. KLEINA, Nilton. História da Apple. História da tecnologia. 2020. Disponível em: < https://www.tecmundo.com.br/videos/Fhvp2tQJK3A-a-historia-da-apple-historia-da-tecnologia.htm >.

1. GARCIA, Fernando Deluno. Breve Histórico da ARM. 22 mar 2017. Disponível em: < https://www.embarcados.com.br/breve-historico-da-arm/ >.

1. Carlos Cardoso, Altair 8800: 45 anos do computador que deu início à Revolução Digital. 2020. Disponível em:  <https://tecnoblog.net/meiobit/433555/altair-8800-45-anos-do-computador-que-deu-inicio-a-revolucao-digital/>

1. Eduardo Sorrentino, Apple 1: O primeiro produto da história da Apple Computer faz 45 anos. 2021. Disponível em: <https://olhardigital.com.br/2021/04/11/reviews/apple-1-o-primeiro-produto-da-historia-da-apple-computers-faz-45-anos/>

1. clássico computador Apple II, 2015-05. Disponível em: <https://www.hardware.com.br/noticias/2015-05/teste-agora-no-seu-computador-um-simulador-do-classico-computador-apple-ii.html>

1. Cesar Cardoso, Apple, ARM e desktops: juntos desde 1987, ou 1990, ou talvez 1997, 2020. Disponível em: <https://www.retrocomputaria.com.br/tag/acorn-archimedes/>