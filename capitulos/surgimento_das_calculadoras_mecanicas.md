# Calculadoras Mecânicas

<img src = "images/abaco.PNG"> 

Existem elementos que são imprescindíveis para o crescimento de uma sociedade, e a matemática é um deles. Essa ciência está presente em todas as situações, seja nas formas dos objetos, nas medidas de comprimento, na escola, em casa, no lazer e nas brincadeiras.

A palavra “computador” vem do verbo “computar” que, por sua vez, significa “calcular”. Sendo assim, podemos pensar que a criação de computadores começa na idade antiga, já que a relação de contar já intrigava os homens.

Dessa forma, uma das primeiras máquinas de computar foi o “**ábaco**”, instrumento mecânico de origem chinesa criado no século VI a.C. Esse instrumento dispunha de fios paralelos e arruelas deslizantes, capazes de realizar contas de adição e subtração. Essa invenção revolucionou a matemática, pois acabou sendo o principal mecanismo de cálculo durante os 24 séculos seguintes. [1].






# Referências:

1. A evolução dos cálculos e das calculadoras. <https://blog.certisign.com.br/a-evolucao-dos-calculos-e-das-calculadoras/>


