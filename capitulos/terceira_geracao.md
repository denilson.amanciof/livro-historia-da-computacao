# Terceira Geração

Os computadores de terceira geração utilizados circuitos integrados (IC's) em lugar de transistores. Um único IC tem vários transistores, resistores e capacitores junto com o circuitos associados.
Foi nesse período que os chips foram criados e a utilização de computadores pessoais iniciou.

</br></br>

## IBM System 360 - (1965)

<img src = "images/ibmsystem_360.jpg"> 

Tendo como responsável por sua arquitetura o projetista Gene Myron Amdahl, a série System da IBM, da família mainframe, foram computadores dos mais bem sucedidos da história. A mais importante ideia por trás do System 360 era que ocupasse menos espaço que os modelos anteriores da marca, tendo um gabinete principal onde os processamentos eram realizados. Outra característica do computador era que os consumidores poderiam posteriormente migrar seu sistema para outro mais avançado, que viria a ser produzido no futuro pela mesma empresa. Este fator fez com que a máquina fosse uma das mais bem sucedidas da história, ainda sendo computadores de grande porte. A padronização de configuração criada com esses modelos da IBM é considerada um marco histórico pois a partir dali a ideia de upgrade de um sistema passa a ser concreta [1]. Vale ressaltar que é entre os usuários dos computadores IBM que nasce o termo PC (personal computer).

</br> </br></br>

## Honeywell Series 6000 - (1970)

<img src = "images/HoneywellSeries 6000.jpg"> 

A série de computadores Honeywell foi uma série desenvolvida a partir de uma linha de computadores já existente de propriedade da multinacional americana General Eletric (GE). A Honeywell Inc. a qual adquiriu a serie 600 da GE repaginou os já consagrados mainframes, sendo sua principal mudança a adição de um conjunto de instruções estendidas (EIS) que fornecia instruções de manipulação de srings e instruções de decimais compactadas [2]. 

</br> </br>

# Referências:

1. KLEINA, Nilton. A história da IBM, uma empresa centenária e revolucionária [em vídeo]. TecMundo. 31 out. 2017. Disponível em: < https://www.tecmundo.com.br/mercado/121588-historia-ibm-empresa-centenaria-revolucionaria-video.htm >.

1. BELLEc, Jean. From GECOS to GCOS8: an history of Large Systems in GE, Honeywell, NEC and Bull. Fédération des Equipes Bull - FEB. 19 fev 2002.

1. Computador - Terceira geração <https://www.tutorialspoint.com/pg/computer_fundamentals/computer_third_generation.htm#:~:text=O%20per%C3%ADodo%20da%20terceira%20gera%C3%A7%C3%A3o,junto%20com%20o%20circuitos%20associados.>

1. IBM ARCHIVES, System 360 <https://www.ibm.com/ibm/history/ibm100/us/en/icons/system360/>

