# Segunda Geração

Ainda com dimensões muito grandes, os computadores da segunda geração funcionavam por meio de transistores, os quais substituíram as válvulas que eram maiores e mais lentas. Nesse período já começam a se espalhar o uso comercial.

</br></br>

## IBM 1620 - (1959)
                
<img src = "images/ibm1620.jpg"> 

Após alguns anos de desenvolvimento e esforço para baratear os custos dos computadores e transformá-los em um produto de massa a IBM trouxe ao mercado do IBM 1620, no ano de 1959, considerado o primeiro computador para o uso científico. Além de fazer uso dos cartões perfurados, um dos produtos mais rentáveis da IBM na época, o que tornava os lucros do 1620 ainda mais altos era o fator de ter um custo de fabricação considervelmente mais barato que os já ultrapassados computadores de uso geral, que ocupavam uma sala. A máquina começou gradualmente a ser instalada em várias universidades americanas com o intuito de servir para armazenamento, análise e processamento de dados. Um fato interessante é que ele foi também o primeiro computador instalado na USP, Universidade de São Paulo [2].

</br> </br></br>

## UNIVAC 1108 - (1964)

<img src = "images/univac1108.jpg">  

As series de computadores UNIVAC, criadas incialmente pro J. Presper Eckert e John Mauchly (o mesmo cientista envolvido no desenvolvimento do ENIAC), estão entre os pioneiros na categoria de computadores comerciais da história [3]. O modelo 1108 se destaca na história por ter sido um dos modelos de mais sucesso usando transistores e circuitos integrados, na época um sistema projetado e pensado para tirar o melhor desempenho possível do hardware com o qual contava. 

</br> </br>

# Referências:

1. SONG, Siang Wun. Computação em SP e no Brasil - desde o
seu início - “causos” e história. X Escola Regional de Computação de Alto Desempenho do Estado de São Paulo
UNICAMP, Campinas, 2019.

1. GRAY, G. T.; SMITH, R. Q. "Sperry Rand's third-generation computers 1964-1980," in IEEE Annals of the History of Computing, vol. 23, no. 1, pp. 3-16, 2001.

1. História e Evolução dos Computadores <https://www.todamateria.com.br/historia-e-evolucao-dos-computadores/>

1. IBM 1620  <https://br.pinterest.com/pin/ibm-1620--266134659199924035/>

1. Computer History Museum <https://www.computerhistory.org/collections/catalog/102621838>

