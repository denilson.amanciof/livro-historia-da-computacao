# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

# Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores]()
    - [Primeira Geração](capitulos/primeira_geracao.md)
    - [Segunda Geração](capitulos/segunda_geracao.md)
1. [Evolução dos Computadores Pessoais e sua Interconexão]()
    - [Terceria Geração](capitulos/terceira_geracao.md)
    - [Quarta Geração](capitulos/quarta_geracao.md)
1. [Computação Móvel](capitulos/Computação movel.md)
1. [Futuro]()



## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279860/avatar.png?width=400)  | Denilson Amancio | denilson.amanciof | [denilson.amanciof@gmail.com](mailto:denilson.amanciof@gmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8332599/avatar.png?width=400)  | Otávio Bender | otavio.bender | [otaviobender@aluno.utfpr.edu.br](mailto:otaviobender@aluno.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279834/avatar.png?width=400)  | Matheus Oliveira | matheusoliveira99 | [matheuso.2020@alunos.utfpr.edu.br](mailto:matheuso.2020@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/2578394/avatar.png?width=100)  | Jeferson Lima | jeferson.lima | [jeferson.lima@utfpr.edu.br](mailto:jeferson.lima@utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279824/avatar.png?width=400)  | Rhomélio Anderson | rhomelio | [rhomelio@alunos.utfpr.edu.br](mailto:rhomelio@alunos.utfpr.edu.br)


------------------------------------
